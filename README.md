# Egg Decoder

This is a Flask web app for discovering when and where your chicken eggs were packaged.

## The Inspiration
One day I was frying eggs, and noticed that the styrofoam carton I just pulled from was covered in cryptic codes. What were these numbers and letters there for, and what could I learn from them? The simplest to understand was the julian date. It consists of 3 digits, and tells you what day of the year (001-365) your eggs were packed. The other notable piece of information was the 'plant code', which identifies the exact plant that packaged the eggs. It took some digging, but I found a USDA AMS (Agricultural Marketing Service) page that served as the front end of a database with information on all of these plants (https://apps.ams.usda.gov/plantbook/Query_Pages/PlantFinder.asp). These codes were used to keep track of plants participating in a voluntary USDA egg grading program.

![front page screenshot](readme_assets/front-page.png)
