#import the sys module so FlaskApp load path can be inserted
import sys

#for some reason, this line is essential when using logging in __init__.py 
#logging.basicConfig(stream=sys.stderr)

#insert FlaskApp load path so mod_wsgi actually knows where to find it when importing
sys.path.insert(0,"/var/www/FlaskApp")

#find out how this line isn't redundant with first line of run.py
from FlaskApp import app as application
