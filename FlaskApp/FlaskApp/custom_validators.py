from wtforms.validators import ValidationError

#define validator class to determine if a zip code exists
class ZipAllocated(object):
    def __init__(self, message=None):
        #use default message if one is not specified
        if not message:
            message = u'Field must be between %i and %i characters long.' % (min, max)
        self.message = message

    def __call__(self, form, field):
        zip = field.data
        #as a simple test, check if zip > 50000
        if int(zip) > 50000:
            raise ValidationError(self.message)
