import requests

def geocode(address, api_key):
    #use mapbox geocoding api
    result = requests.get(f"https://api.mapbox.com/geocoding/v5/mapbox.places/{address}.json?access_token={api_key}")
    #return coordinates from result
    return result.json()['features'][0]['center'][0:2]

def route(coord_string, api_key):
    #use mapbox navigation api
    route_to_plant = requests.get(f"https://api.mapbox.com/directions/v5/mapbox/driving/{coord_string}.json?access_token={api_key}")
    #get distance from api result
    route_distance = route_to_plant.json()['routes'][0]['distance']
    #convert from meters to miles
    return round(route_distance * 0.0006213712)
