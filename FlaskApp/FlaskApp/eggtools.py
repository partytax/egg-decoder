import datetime as dt

def date_processor(printed_date, threshold=28):
    """This function takes the 3 digit day of the year
    from an egg carton, and returns a dictionary with
    the pack date, today's date, the expiration date,
    and the number of days between now and expiration."""
    #set variable to today's julian date
    today = dt.datetime.today()
    today_julian = today.timetuple().tm_yday
    #set format to feed julian date and year into parser
    format = '%j%Y'
    #check to see if incoming julian date is before today's
    #if not, use last year instead of this year when doing calculations
    if int(printed_date) < today_julian:
        #create date string with julian date and this year
        datestring = str(printed_date)+str(today.year)
    else:
        #create date string with julian date and last year
        datestring = str(printed_date)+str(today.year-1)
    #create datetime object representing the pack date
    pack_date = dt.datetime.strptime(datestring, format)
    #calculate days since packing
    diff = today - pack_date
    days = diff.days
    #calculate whether eggs are good based on threshold
    if days < threshold:
        good = True
    else:
        good = False
    #calculate expiration date by adding threshold to pack date
    exp_date = pack_date + dt.timedelta(threshold)
    #return relevant info in dictionary
    return {'pack_date':pack_date.isoformat()[:10],
    'today':today.isoformat()[:10],
    'exp_date':exp_date.isoformat()[:10],
    'days':days, 'good':good, 'threshold':threshold, 'date_code':printed_date}

#result = input('enter julian')
#print(date_processor(result))
