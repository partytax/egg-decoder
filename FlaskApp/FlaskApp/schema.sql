CREATE TABLE zips(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  code Integer,
  location POINT
);

CREATE TABLE plants(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  code TEXT,
  company_name TEXT,
  street_address TEXT,
  city_state_zip TEXT,
  phone TEXT,
  fax TEXT,
  service_type TEXT,
  product TEXT,
  import_date INTEGER,
  extant INTEGER
  lat FLOAT,
  lon FLOAT
);
'''
CREATE TABLE visits(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  plant_code TEXT
    REFERENCES plants(code),
  zip_code INTEGER
    REFERENCES zips(code),
  visit_date INTEGER
);
'''
